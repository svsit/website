#!/bin/bash
##
# Generate HTML redirect tags in the directory path provided.

set -eo pipefail

if [[ $# -ne 1 ]]; then
    echo 'Expected 1 argument: WEBROOT' >&2
    exit 1
fi

declare WEBROOT="$1"
declare -A redirects=(
    ['/webshop']='https://crm.svsit.nl/webshop/'
    ['/contact']='https://crm.svsit.nl/contact/'
    ['/user/login']='https://crm.svsit.nl/user/login'
    ['/register']='https://crm.svsit.nl/register'
)

for route in "${!redirects[@]}"; do
    mkdir -vp "$WEBROOT/$route"

    echo " => Generating redirect \"$route\" -> \"${redirects[$route]}\""
    cat <<EOF > "$WEBROOT/$route/index.html"
<!DOCTYPE HTML>
<html>
    <meta http-equiv="refresh" content="0;URL=${redirects[$route]}" />
</html>
EOF
done
