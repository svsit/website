---
date: 2018-02-01T00:00:00-08:00
title: Aan de slag met Suus
---

Hoi SIT-studenten, sinds april 2017 schrijf ik een maandelijkse column voor jullie site. 

## Even voorstellen

Ik ben Suus en psycholoog voor alle studenten in en rond Amsterdam. Om me heen zie ik steeds meer studenten die kampen met bijvoorbeeld studiestress, presentatie-angst of somberheid. Gelukkig is er aan veel problemen die studenten ervaren iets te doen. 

Via de columns (te vinden bij 'nieuws') zal ik allerlei stress- en studiegerelateerde thema's bespreken die hierbij kunnen helpen. Mocht je nog een idee voor een column hebben of iets meer willen weten over een bepaald onderwerp, neem dan zeker even contact op via info@aandeslagmetsuus.nl of 06-26792405. 

Voor meer informatie over stress onder studenten, kun je ook altijd een kijkje nemen op mijn website: https://aandeslagmetsuus.nl/.