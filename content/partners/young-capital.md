---
date: 2018-02-01T00:00:00-08:00
title: Young Capital
---

Website: [www.youngcapital.nl](http://www.youngcapital.nl/professionals/it-traineeships)

YoungCapital Professionals staat voor jong talent: het kapitaal van elke organisatie die voorop wil lopen. Wij helpen jonge IT-talenten zoals jij hun plek te veroveren op de arbeidsmarkt!

Gaat jouw hart sneller kloppen van het bouwen of testen van apps, websites of backend applicaties of ligt jouw interesse bij het analyseren van grote hoeveelheden data? Dat komt goed uit! Want via onze IT-traineeships kun je daar je werk van maken en direct aan de slag als bijvoorbeeld developer, BI/big data-engineer of software tester.

**Dit bieden wij jou**

-   een carrièrestart als IT-professional;
-   een intensieve training van twee maanden, inclusief het behalen van certificaten (bv. Scrum, TMap Suite, OCP- of MCSD-examen);
-   baangarantie binnen de IT;
-   een opdrachtgever bij jou in de buurt;
-   een marktconform salaris vanaf je eerste werkdag.

**Dit vragen wij van jou**

Een afgeronde opleiding én een dosis enthousiasme om jouw IT carrière te starten!

**Interesse?**

Kijk op onze [website](http://www.youngcapital.nl/professionals/it-traineeships) voor meer informatie of neem contact op met Marlies Wardenier, te bereiken via 020-311 82 90 of [it-traineeship@youngcapital.nl](mailto:it-traineeship@youngcapital.nl).
