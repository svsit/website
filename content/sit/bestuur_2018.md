---
title: "Bestuur 2017 - 2018"
date: 2018-04-08T09:16:58-08:00
layout: subsection
slug: bestuur-2018
weight: 101
---

![Groepsfoto](https://svsit.nl/uploads/bestuursleden/20170722-IMG_0714_1506600421.JPG)

---

![Foto](https://svsit.nl/media/cache/bestuuricon/uploads/bestuursleden/Mathijs_1506600071.jpg)

Mathijs Marks

Voorzitter

![Foto](https://svsit.nl/media/cache/bestuuricon/uploads/bestuursleden/Joshua_1506600219.jpg)

Joshua Turpijn

Vice-Voorzitter

![Foto](https://svsit.nl/media/cache/bestuuricon/uploads/bestuursleden/Laurens_1506599946.jpg)

Laurens Nieuwendijk

Penningmeester/Commisaris

![Foto](https://svsit.nl/media/cache/bestuuricon/uploads/bestuursleden/Thomas_1506599525.jpg)

Thomas Haagsma

Secretaris