---
title: "Nieuwsbrief SIT: mei"
date: 2018-05-01
tags:
- nieuwsbrief
---

Je bent begonnen met het lezen van de nieuwe maandelijkse nieuwsbrief van Studievereniging Innovatie & Technologie. In deze nieuwsbrief vind je aankondigingen, informatie over evenementen van SIT, relevant ICT nieuws en communicatie vanuit het bestuur.  
  
# Aankondigingen

![](/bc5372c3-0c16-4738-bc90-6cd93005ab92.png)

## Spoed ALV

SIT'ers, de eindbazen in games zijn altijd het lastigst, de laatste periode hebben wij geprobeerd aan te poten, maar het bleef veel werk. Vandaar dat wij genoodzaakt zijn om een SPOED-ALV aan te vragen. Op deze spoed-alv zullen wij de nieuwe SOCO(Sollicitatie commissie) instemmen! Met een nieuwe SOCO kunnen wij beginnen met het verwerkingsproces van de aanmeldingen voor Bestuur IV van SIT!  
  
Bij deze willen we jullie graag vragen om jullie aanmelding voor Bestuur IV naar [soco@svsit.nl](mailto:soco@svsit.nl) te sturen. Deze aanmeldingen zullen niet bezichtigd worden, totdat de nieuwe SOCO is ingestemd. De absolute einddatum voor de aanmeldingen zal nog komen, maar probeer alvast een goede motivatie op te sturen naar [soco@svsit.nl](mailto:soco@svsit.nl).  
  
[soco@svsit.nl](mailto:soco@svsit.nl). <- Daarheen sturen. Alsjeblieft. Dank je.

# Evenementen van SIT

![](https://gallery.mailchimp.com/f3087c0ae44c40d10a3ea3866/images/4ac70006-02be-476d-af97-5f2f574793d0.jpg)

## Algemene ledenvergadering

Op donderdag 7 juni staat er weer een Algemene Ledenvergadering op de planning. Bij deze ALV zal bestuur IV worden voorgelegd aan de leden.  
  
Belangrijke informatie:  
Tijd: 15:30 t/m 17:30  
Locatie: Common Room, Kohnstammhuis 00A07, Wibautstraat 2-4, 1091 GM Amsterdam  
Datum: 7-6-2018

![](https://gallery.mailchimp.com/f3087c0ae44c40d10a3ea3866/images/c2226796-3ba3-45ff-ad23-673a380f5a1d.png)

# SIT BBQ

Om het jaar mooi af te sluiten is op donderdag 7 juni na de ALV een BBQ van SIT op het schoolplein voor het TTH.  
  
Het aanmeldingsformulier voor deze BBQ volgt snel. Dus zet de datum alvast in je agenda!  
  
Belangrijke informatie:  
Tijd: 18:00 t/m 21:00  
Locatie: Schoolplein voor het Theo Thijsen Huis (TTH)  
Datum: 7-6-2018

![](https://gallery.mailchimp.com/f3087c0ae44c40d10a3ea3866/images/f52e63ff-c37c-4834-9144-f5fee9d79932.jpg)

# Lezing: Command-line interfaces in combinatie met Shells

Binnen de ICT wereld zijn er tientallen omgevingen waarop wij ons werk kunnen doen. Een veelgebruikte omgeving is de vorm van Command-line Interfaces. Zo wordt er, door middel van query's, commando’s gestuurd naar het besturingssysteem. Om deze commando’s te sturen naar het besturingssysteem wordt er gebruik gemaakt van Shells.  
  
Hugo Thunnissen, één van de actieve leden van SIT, weet veel over dit onderwerp. Vandaar dat de volgende SIT lezing wordt verzorgd door Hugo met als onderwerp: Command-line Interfaces in combinatie met Shells.  
  
De lezing zal worden gegeven op maandag 7 mei 2018 van 18:00 t/m 19:00, in de collegezaal naast de Common Room in het Kohnstammhuis.  
  
Als je meer wilt weten. Kom zeker langs!  
  
Belangrijke informatie:  
Datum: 7-5-2018  
Locatie: Collegezaal naast de Common Room (KSH 00A07)  
Tijd: 18:00 t/m 19:00

![](https://gallery.mailchimp.com/f3087c0ae44c40d10a3ea3866/images/0bc96fe9-874a-43df-ba80-f4ecc208421f.jpg)

# Boardgamenight

De boardgamecommissie heeft op dinsdag 8-5-18 en dinsdag 22-5-18 weer een boardgamenight op de planning staan. Kom samen met SIT genieten van ouderwetse gezelligheid tijdens het spelen van allerlij bordspellen.  
  
Belangrijke informatie:  
Tijd: 18:00 t/m 22:00  
Locatie: Theo Thijsen Huis (TTH), studielandschap op de 3e verdieping.  
Data: 8-5-2018 | 22-5-2018

![](/b5848b09-712d-4388-adda-30c99f71c159.jpg)

# Breek de Week!

Na weer een week school is het ook deze week weer tijd voor breek de week! Een avond vol gezelligheid, bier en natuurlijk lekker bijpraten met de studievereniging.  
  
Je bent elke donderdag weer welkom in Café Fest.

  
Belangrijke informatie:  
Tijd: 20:00 t/m 23:59  
Locatie: Café Fest, Wibauthof 1, 1091 DD Amsterdam, Netherlands.  
Datum: Iedere donderdag avond

![](/63a34bdb-6aad-47e6-980c-e78fe23d0cf9.png)

# Gamejam Cloud Games!

Na de succesvolle lezing van Cloud Games in november van vorig kalenderjaar is SIT nog volop in zee met Cloud Games. Daarom organiseren wij samen met Cloud Games een gamejam bij hun op kantoor.  
  
De games worden gemaakt op het platform van Facebook Messenger met HTML5.  
Het thema van deze gamejam is daarom ook: Messengerjam  
  
Inschrijven wordt binnenkort beschikbaar, heb je wel al interesse? Laat je naam achter in dit google form: https://goo.gl/forms/iKX88QoLzSrCNJUh1
  
Belangrijke informatie:  
Tijd: Vrijdag 1 juni vanaf 16:00 t/m zondag 3 juni om 16:00  
Locatie: Kantoor Cloud Games, Stationsplein, Haarlem  
Datum: 1, 2 en 3 juni

IT nieuws

![](https://gallery.mailchimp.com/f3087c0ae44c40d10a3ea3866/images/0c4d6e48-b8d6-4385-8f07-c26660a263ec.jpg)

# Russia bans 50 VPNs and proxy servers in effort to block Telegram users

Telegram is an encrypted messaging service that has recently become popular, especially in countries with restricted speech such as Iran and Russia.

In April, [Russia requested](https://svsit.us16.list-manage.com/track/click?u=f3087c0ae44c40d10a3ea3866&id=c01d7742fc&e=199d65a189) that Google and Apple remove the Telegram app from their respective regional stores. The move followed a court order that blocked the app for failing to provide encryption keys to agencies such as Russia's Federal Security Service so they could easily access user messages. Telegram tried to circumvent the order by moving at least some of its services to different host servers.

Russia responded by blocking more than 15.8 million IP addresses, which hampered other services such as game servers, banking, retail, and cryptocurrency websites. According to self-proclaimed Russian news outlet [Meduza](https://svsit.us16.list-manage.com/track/click?u=f3087c0ae44c40d10a3ea3866&id=7b9a696999&e=199d65a189), the country is blocking another 50 services consisting of VPNs and web anonymizers in an effort to thwart circumvention of the sweeping bans.

The government’s agency in charge of censorship Roskomnadzor did not disclose which services it was blocking. The only thing the it would confirm is that Viber, another popular messaging platform, would not be targeted in the embargo.

“Since April 16, the Russian authorities have blocked roughly 20 million IP addresses, including servers operated by Google, Amazon, Microsoft, and Digital Ocean,” says Meduza. “Roskomnadzor’s crackdown has disrupted a wide range of unrelated online services that rely on cloud computing hosted on blocked servers.”

Google and Apple have not commented on whether or not they will comply with requests and it is unknown what actions Roskomnadzor can take if they refuse to remove the apps. Even if the giant US corporations acquiesce to Russia’s wishes, users are likely to find ways around the restrictions.

“[Despite the bans] Telegram has remained accessible to most Russian Internet users by utilizing a variety of circumvention tricks,” says Meduza.

According to the messaging service, its app has at least 15 million users in Russia, and 10 million open the app daily. It is third only to [WhatsApp](https://svsit.us16.list-manage.com/track/click?u=f3087c0ae44c40d10a3ea3866&id=36dc1c2960&e=199d65a189) (25 million users) and [Viber](https://svsit.us16.list-manage.com/track/click?u=f3087c0ae44c40d10a3ea3866&id=88517c26ab&e=199d65a189) (21 million).

  
  
  
Bron: https://www.techspot.com/news/74461-russia-bans-50-vpns-proxy-servers-effort-block.html

Communicatie vanuit bestuur

![](https://gallery.mailchimp.com/f3087c0ae44c40d10a3ea3866/images/cb3080d6-b526-47b7-8f94-64cc8ccb8a4f.jpg)

# Ontwikkelingen

Het bestuur is afgelopen maand druk bezig met de planning voor het einde van het schooljaar. Zo hebben wij nog een aantal evenementen op de planning gezet waaronder de lezing van Hugo en de SIT BBQ. Verder is het bestuur druk bezig met de overdracht voor bestuur IV.

# Aanpak volgende maand

Voor de maand mei wilt het bestuur zich focussen op het werven van bestuur IV. Hieronder valt onder andere het opzetten van de nieuwe SoCo. Hiernaast wilt het bestuur de BBQ goed aanpaken dit jaar, dus de voorbereiding loopt al gesmeerd!
