---
date: 2018-03-01
draft: false
title: Welkom
---

![Logo](https://svsit.nl/bundles/sit/images/logo_large.png)

---

SIT organiseert verschillende studie gerelateerde evenementen. Zo organiseert SIT lezingen over verschillende thema’s binnen de IT. Ook worden er diversen workshops en soft skills traingen gegeven.

De game commissie organiseert van alles voor de gamer. Zo zijn er in het verleden al verschillende game toernooien gehouden met o.a. League of Legends, Mario Kart, Super Smash en nog veel meer. De Game toernooien worden om de week op woensdag georganiseerd en vinden plaats in het TTH op de 4e verdieping. Ook organiseert de board game commissie om de week op dinsdag een bordspellen avond. Hier spelen we bordspelletjes zoals Coup, Weerwolven en nog veel meer! De bordspellen avond in het TTH op de 3e verdieping.

Elke week op donderdag avond organiseert SIT een borrel in Café Fest. De borrels zijn natuurlijk een geweldige manier om je medestudenten te leren kennen of natuurlijk om gewoon gezellig een drankje te doen met de gezelligste studievereniging van de HvA!
